package airalo.assignment.common_navigation

import airalo.assignment.common_navigation.di.RouterHolder
import androidx.fragment.app.Fragment
import com.github.terrakok.cicerone.Router

inline val Fragment.router: Router
    get() = (requireContext().applicationContext as RouterHolder).router
