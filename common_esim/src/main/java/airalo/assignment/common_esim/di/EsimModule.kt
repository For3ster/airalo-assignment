package airalo.assignment.common_esim.di

import airalo.assignment.domain_esim.EsimApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

private const val BASE_URL = "https://www.airalo.com/api/v2/"

@Module
object EsimModule {

    @Provides
    @Singleton
    fun provideEsimApi(
        builder: Retrofit.Builder,
    ): EsimApi =
        builder
            .baseUrl(BASE_URL)
            .build()
            .create(EsimApi::class.java)
}
