package airalo.assignment.feature_package.presentation

import airalo.assignment.domain_esim.dto.CountryPackage
import airalo.assignment.feature_package.presentation.PackageCommand.GetPackages
import airalo.assignment.feature_package.presentation.PackageEffect.ShowError
import airalo.assignment.feature_package.presentation.PackageEvent.Internal
import airalo.assignment.feature_package.presentation.PackageEvent.Ui
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import org.mockito.kotlin.mock

internal class PackageReducerTest : BehaviorSpec({
    val reducer = PackageReducer

    Given("Initial State") {
        When("Ui.System.Init") {
            val (state, effects, commands) = reducer.reduce(Ui.System.Init, STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") {
                commands.shouldContainExactly(GetPackages(COUNTRY_ID))
            }
        }

        When("Internal.GetPackagesSuccess") {
            val (state, effects, commands) =
                reducer.reduce(Internal.GetPackagesSuccess(COUNTRY_PACKAGE), STATE)

            Then("check state") {
                state.countryPackage shouldBe COUNTRY_PACKAGE
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.GetPackagesError") {
            val (state, effects, commands) =
                reducer.reduce(Internal.GetPackagesError(ERROR), STATE)

            Then("check state") {
                state.countryPackage.shouldBeNull()
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldContainExactly(ShowError(ERROR)) }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }
}) {

    private companion object {

        const val COUNTRY_ID = 1
        val STATE = PackageState(COUNTRY_ID)
        val ERROR = Throwable()
        val COUNTRY_PACKAGE: CountryPackage = mock()
    }
}
