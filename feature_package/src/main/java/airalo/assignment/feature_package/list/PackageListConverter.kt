package airalo.assignment.feature_package.list

import airalo.assignment.feature_package.presentation.PackageState

internal object PackageListConverter {

    fun map(state: PackageState): List<PackageItem> {
        if (state.countryPackage == null) return emptyList()
        return state.countryPackage.packages.map {
            PackageItem(
                id = it.id,
                operator = it.operator.title,
                country = state.countryPackage.title,
                price = it.price,
                data = it.data,
                validity = it.validity,
                gradientStart = it.operator.gradientStart,
                gradientEnd = it.operator.gradientEnd,
                imageUrl = it.operator.image.url
            )
        }
    }
}
