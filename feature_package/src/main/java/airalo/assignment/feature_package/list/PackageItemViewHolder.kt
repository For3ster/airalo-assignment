package airalo.assignment.feature_package.list

import airalo.assignment.feature_package.R
import airalo.assignment.feature_package.databinding.ItemPackageBinding
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.TypedValue
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.RoundedCornersTransformation

internal class PackageItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemPackageBinding.bind(itemView)

    fun bind(item: PackageItem) = with(binding) {
        val gradientStart = Color.parseColor(item.gradientStart)
        val gradientEnd = Color.parseColor(item.gradientEnd)
        val textColor = if (ColorUtils.calculateLuminance(gradientStart) < 0.25) {
            ContextCompat.getColor(itemView.context, R.color.textLight)
        } else {
            ContextCompat.getColor(itemView.context, R.color.textDark)
        }

        operator.setTextColor(textColor)
        country.setTextColor(textColor)
        dataTitle.setTextColor(textColor)
        dataValue.setTextColor(textColor)
        validityTitle.setTextColor(textColor)
        validityValue.setTextColor(textColor)
        buyButton.setTextColor(textColor)

        TextViewCompat.setCompoundDrawableTintList(dataTitle, ColorStateList.valueOf(textColor))
        TextViewCompat.setCompoundDrawableTintList(validityTitle, ColorStateList.valueOf(textColor))

        val drawable = GradientDrawable().apply {
            colors = intArrayOf(gradientStart, gradientEnd)
            orientation = GradientDrawable.Orientation.LEFT_RIGHT
            gradientType = GradientDrawable.LINEAR_GRADIENT
            shape = GradientDrawable.RECTANGLE
            cornerRadius = cardBackground.dpToPx(dp = 8)
        }
        cardBackground.background = drawable

        operator.text = item.operator
        country.text = item.country
        dataValue.text = item.data
        validityValue.text = item.validity
        buyButton.text = itemView.context.getString(R.string.button_buy).format(item.price)

        operatorImage.load(item.imageUrl) {
            transformations(RoundedCornersTransformation(radius = operatorImage.dpToPx(dp = 14)))
        }
    }
}

private fun View.dpToPx(dp: Int): Float =
    TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp.toFloat(),
        resources.displayMetrics
    )
