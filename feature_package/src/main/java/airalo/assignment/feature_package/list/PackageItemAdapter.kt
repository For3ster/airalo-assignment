package airalo.assignment.feature_package.list

import airalo.assignment.feature_package.databinding.ItemPackageBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

internal class PackageItemAdapter :
    ListAdapter<PackageItem, PackageItemViewHolder>(ItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PackageItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPackageBinding.inflate(inflater, parent, false)

        return PackageItemViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: PackageItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private class ItemDiffCallback : DiffUtil.ItemCallback<PackageItem>() {

    override fun areItemsTheSame(oldItem: PackageItem, newItem: PackageItem): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: PackageItem, newItem: PackageItem): Boolean =
        oldItem.data == newItem.data
}
