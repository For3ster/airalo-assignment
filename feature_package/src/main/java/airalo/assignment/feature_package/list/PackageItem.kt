package airalo.assignment.feature_package.list

data class PackageItem(
    val id: Int,
    val operator: String,
    val country: String,
    val price: Double,
    val data: String,
    val validity: String,
    val gradientStart: String,
    val gradientEnd: String,
    val imageUrl: String,
)
