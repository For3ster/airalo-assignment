package airalo.assignment.feature_package.presentation

import airalo.assignment.feature_package.presentation.PackageEvent.Internal
import airalo.assignment.feature_package.presentation.PackageEvent.Ui
import vivid.money.elmslie.core.store.dsl_reducer.ScreenDslReducer
import airalo.assignment.feature_package.presentation.PackageCommand as Command
import airalo.assignment.feature_package.presentation.PackageEffect as Effect
import airalo.assignment.feature_package.presentation.PackageEvent as Event
import airalo.assignment.feature_package.presentation.PackageState as State

internal object PackageReducer : ScreenDslReducer<Event, Ui, Internal,
        State, Effect, Command>(Ui::class, Internal::class) {

    override fun Result.ui(event: Ui) = when (event) {
        is Ui.System.Init -> commands { +Command.GetPackages(state.countryId) }
    }

    override fun Result.internal(event: Internal) = when (event) {
        is Internal.GetPackagesSuccess -> state {
            copy(countryPackage = event.countryPackage, isLoading = false)
        }

        is Internal.GetPackagesError -> {
            state { copy(isLoading = false) }
            effects { +Effect.ShowError(event.error) }
        }
    }
}
