package airalo.assignment.feature_package.presentation

import vivid.money.elmslie.coroutines.ElmStoreCompat
import javax.inject.Inject

interface PackageStoreFactory {

    fun create(countryId: Int): PackageStore
}

internal class PackageStoreFactoryImpl @Inject constructor(
    private val actor: PackageActor,
) : PackageStoreFactory {

    override fun create(countryId: Int): PackageStore = ElmStoreCompat(
        initialState = PackageState(countryId),
        reducer = PackageReducer,
        actor = actor
    )
}
