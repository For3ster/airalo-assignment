package airalo.assignment.feature_package.presentation

import airalo.assignment.common_mvi.actorFlow
import airalo.assignment.domain_esim.EsimApi
import airalo.assignment.feature_package.presentation.PackageCommand.GetPackages
import airalo.assignment.feature_package.presentation.PackageEvent.Internal
import kotlinx.coroutines.flow.Flow
import vivid.money.elmslie.coroutines.Actor
import javax.inject.Inject

internal class PackageActor @Inject constructor(
    private val esimApi: EsimApi,
) : Actor<PackageCommand, Internal> {

    override fun execute(command: PackageCommand): Flow<Internal> =
        when (command) {
            is GetPackages -> actorFlow {
                esimApi.getPackages(command.countryId)
            }.mapEvents(Internal::GetPackagesSuccess, Internal::GetPackagesError)
        }
}
