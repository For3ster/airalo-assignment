package airalo.assignment.feature_package.presentation

import airalo.assignment.domain_esim.dto.CountryPackage
import vivid.money.elmslie.core.store.Store

typealias PackageStore = Store<PackageEvent, PackageEffect, PackageState>

data class PackageState(
    val countryId: Int,
    val countryPackage: CountryPackage? = null,
    val isLoading: Boolean = true,
)

sealed interface PackageEvent {

    sealed interface Ui : PackageEvent {
        object System {
            object Init : Ui
        }
    }

    sealed interface Internal : PackageEvent {
        data class GetPackagesSuccess(val countryPackage: CountryPackage) : Internal
        data class GetPackagesError(val error: Throwable) : Internal
    }
}

sealed interface PackageEffect {
    data class ShowError(val error: Throwable) : PackageEffect
}

sealed interface PackageCommand {
    data class GetPackages(val countryId: Int) : PackageCommand
}
