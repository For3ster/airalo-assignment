package airalo.assignment.feature_package.di

import airalo.assignment.feature_package.presentation.PackageStoreFactory

interface PackageDependencies {

    val packageStoreFactory: PackageStoreFactory
}
