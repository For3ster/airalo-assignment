package airalo.assignment.feature_package.di

import airalo.assignment.domain_package.feature_api.PackageFeatureApi
import airalo.assignment.feature_package.feature_api.PackageFeatureApiImpl
import airalo.assignment.feature_package.presentation.PackageStoreFactory
import airalo.assignment.feature_package.presentation.PackageStoreFactoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class PackageModule {

    @Binds
    internal abstract fun PackageFeatureApiImpl.bindPackageFeatureApi(): PackageFeatureApi

    @Binds
    internal abstract fun PackageStoreFactoryImpl.bindPackageStoreFactory(): PackageStoreFactory
}
