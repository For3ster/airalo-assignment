package airalo.assignment.feature_package.feature_api

import airalo.assignment.domain_package.feature_api.PackageFeatureApi
import airalo.assignment.feature_package.PackageFragment
import com.github.terrakok.cicerone.Screen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import javax.inject.Inject

internal class PackageFeatureApiImpl @Inject constructor() : PackageFeatureApi {

    override fun getScreen(countryId: Int): Screen =
        FragmentScreen { PackageFragment.newInstance(countryId) }
}
