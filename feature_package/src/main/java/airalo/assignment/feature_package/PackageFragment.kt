package airalo.assignment.feature_package

import airalo.assignment.common_di.getComponent
import airalo.assignment.common_mvi.base.BaseFragment
import airalo.assignment.common_mvi.setOnBackPressedCallback
import airalo.assignment.common_navigation.router
import airalo.assignment.feature_package.databinding.FragmentPackageBinding
import airalo.assignment.feature_package.di.PackageDependencies
import airalo.assignment.feature_package.list.PackageItem
import airalo.assignment.feature_package.list.PackageItemAdapter
import airalo.assignment.feature_package.list.PackageListConverter
import airalo.assignment.feature_package.presentation.PackageEffect
import airalo.assignment.feature_package.presentation.PackageEvent.Ui
import airalo.assignment.feature_package.presentation.PackageState
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import vivid.money.elmslie.android.storeholder.LifecycleAwareStoreHolder
import vivid.money.elmslie.android.storeholder.StoreHolder
import airalo.assignment.feature_package.presentation.PackageEffect as Effect
import airalo.assignment.feature_package.presentation.PackageEvent as Event
import airalo.assignment.feature_package.presentation.PackageState as State

internal const val ARG_COUNTRY_ID = "ARG_COUNTRY_ID"

internal class PackageFragment : BaseFragment<Event, Effect, State>(R.layout.fragment_package) {

    private val countryId: Int by lazy { arguments?.getInt(ARG_COUNTRY_ID)!! }
    private val component by lazy { getComponent<PackageDependencies>() }

    override val initEvent: Event = Ui.System.Init

    override val storeHolder: StoreHolder<Event, Effect, State> =
        LifecycleAwareStoreHolder(
            lifecycle = lifecycle,
            storeProvider = { component.packageStoreFactory.create(countryId) }
        )

    private val binding by viewBinding(FragmentPackageBinding::bind)
    private lateinit var adapter: PackageItemAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.adapter = PackageItemAdapter().also { adapter = it }

        binding.toolbar.setNavigationOnClickListener { router.exit() }
        setOnBackPressedCallback { router.exit() }
    }

    override fun render(state: PackageState) {
        binding.progressView.root.isVisible = state.isLoading
        state.countryPackage?.title?.let { binding.toolbarTitle.text = it }
    }

    override fun mapList(state: PackageState): List<PackageItem> =
        PackageListConverter.map(state)

    @Suppress("UNCHECKED_CAST")
    override fun renderList(state: PackageState, list: List<Any>) {
        adapter.submitList(list as List<PackageItem>)
    }

    override fun handleEffect(effect: PackageEffect) = when (effect) {
        is PackageEffect.ShowError -> showError(effect.error.toString())
    }

    companion object {

        fun newInstance(countryId: Int): Fragment = PackageFragment().apply {
            arguments = bundleOf().apply { putInt(ARG_COUNTRY_ID, countryId) }
        }
    }
}
