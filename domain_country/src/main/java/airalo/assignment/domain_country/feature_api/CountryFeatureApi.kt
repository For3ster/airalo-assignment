package airalo.assignment.domain_country.feature_api

import com.github.terrakok.cicerone.Screen

interface CountryFeatureApi {

    fun getScreen(): Screen
}
