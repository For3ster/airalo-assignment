package airalo.assignment.domain_esim.dto

@JvmInline
value class Image(
    val url: String,
)
