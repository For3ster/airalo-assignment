package airalo.assignment.domain_esim.dto

import com.squareup.moshi.Json

data class Package(
    val id: Int,
    val price: Double,
    val data: String,
    val validity: String,
    val operator: Operator,
)

data class Operator(
    val id: Int,
    val title: String,
    @Json(name = "gradient_start")
    val gradientStart: String,
    @Json(name = "gradient_end")
    val gradientEnd: String,
    val image: Image,
)

data class CountryPackage(
    val title: String,
    val packages: List<Package>,
)
