package airalo.assignment.domain_esim.dto

data class Country(
    val id: Int,
    val title: String,
    val image: Image,
)

