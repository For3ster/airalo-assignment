package airalo.assignment.domain_esim

import airalo.assignment.domain_esim.dto.Country
import airalo.assignment.domain_esim.dto.CountryPackage
import retrofit2.http.GET
import retrofit2.http.Path

private const val COUNTRY_ID_PATH = "countryId"

interface EsimApi {

    @GET("countries")
    suspend fun getCountries(): List<Country>

    @GET("countries/{$COUNTRY_ID_PATH}")
    suspend fun getPackages(
        @Path(COUNTRY_ID_PATH) countryId: Int,
    ): CountryPackage
}
