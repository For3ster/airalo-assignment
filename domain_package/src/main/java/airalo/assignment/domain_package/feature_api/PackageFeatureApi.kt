package airalo.assignment.domain_package.feature_api

import com.github.terrakok.cicerone.Screen

interface PackageFeatureApi {

    fun getScreen(countryId: Int): Screen
}
