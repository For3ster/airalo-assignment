package airalo.assignment

import airalo.assignment.common_di.ComponentHolder
import airalo.assignment.common_navigation.di.RouterHolder
import airalo.assignment.di.AppComponent
import airalo.assignment.di.DaggerAppComponent
import android.app.Application
import com.github.terrakok.cicerone.Router

class App : Application(),
    ComponentHolder<Any>,
    RouterHolder {

    lateinit var appComponent: AppComponent

    override val router: Router
        get() = appComponent.router

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.factory().create(this, this)
    }

    override fun getComponent(componentClass: Class<out Any>): Any =
        if (componentClass.isInstance(appComponent)) {
            appComponent
        } else {
            error("Component $componentClass is not declared")
        }
}
