package airalo.assignment.di

import airalo.assignment.common_esim.di.EsimModule
import airalo.assignment.common_navigation.di.NavigationModule
import airalo.assignment.common_network.di.NetworkModule
import airalo.assignment.feature_country.di.CountryDependencies
import airalo.assignment.feature_country.di.CountryModule
import airalo.assignment.feature_package.di.PackageDependencies
import airalo.assignment.feature_package.di.PackageModule
import android.app.Application
import android.content.Context
import dagger.BindsInstance
import dagger.Component

import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NavigationModule::class,
        NetworkModule::class,
        EsimModule::class,
        CountryModule::class,
        PackageModule::class,
    ]
)
interface AppComponent :
    MainDependencies,
    CountryDependencies,
    PackageDependencies {

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance context: Context,
            @BindsInstance application: Application
        ): AppComponent
    }
}
