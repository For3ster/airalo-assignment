package airalo.assignment.di

import airalo.assignment.domain_country.feature_api.CountryFeatureApi
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router

interface MainDependencies {

    val router: Router
    val navHolder: NavigatorHolder
    val countryFeatureApi: CountryFeatureApi
}
