package airalo.assignment.feature_country.feature_api

import airalo.assignment.domain_country.feature_api.CountryFeatureApi
import airalo.assignment.feature_country.CountryFragment
import com.github.terrakok.cicerone.Screen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import javax.inject.Inject

internal class CountryFeatureApiImpl @Inject constructor() : CountryFeatureApi {

    override fun getScreen(): Screen =
        FragmentScreen { CountryFragment.newInstance() }
}
