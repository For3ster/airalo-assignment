package airalo.assignment.feature_country.presentation

import vivid.money.elmslie.coroutines.ElmStoreCompat
import javax.inject.Inject

interface CountryStoreFactory {

    fun create(): CountryStore
}

internal class CountryStoreFactoryImpl @Inject constructor(
    private val actor: CountryActor,
) : CountryStoreFactory {

    override fun create(): CountryStore = ElmStoreCompat(
        initialState = CountryState(),
        reducer = CountryReducer,
        actor = actor
    )
}
