package airalo.assignment.feature_country.presentation

import airalo.assignment.feature_country.presentation.CountryEvent.Internal
import airalo.assignment.feature_country.presentation.CountryEvent.Ui
import vivid.money.elmslie.core.store.dsl_reducer.ScreenDslReducer
import airalo.assignment.feature_country.presentation.CountryCommand as Command
import airalo.assignment.feature_country.presentation.CountryEffect as Effect
import airalo.assignment.feature_country.presentation.CountryEvent as Event
import airalo.assignment.feature_country.presentation.CountryState as State

internal object CountryReducer : ScreenDslReducer<Event, Ui, Internal,
        State, Effect, Command>(Ui::class, Internal::class) {

    override fun Result.ui(event: Ui) = when (event) {
        is Ui.System.Init -> commands { +Command.GetCountries }
        is Ui.Click.CountryItem -> effects { +Effect.OpenPackages(event.id) }
    }

    override fun Result.internal(event: Internal) = when (event) {
        is Internal.GetCountriesSuccess -> state {
            copy(countries = event.countries, isLoading = false)
        }

        is Internal.GetCountriesError -> {
            state { copy(isLoading = false) }
            effects { +Effect.ShowError(event.error) }
        }
    }
}
