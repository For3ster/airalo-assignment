package airalo.assignment.feature_country.presentation

import airalo.assignment.common_mvi.actorFlow
import airalo.assignment.domain_esim.EsimApi
import airalo.assignment.feature_country.presentation.CountryCommand.GetCountries
import airalo.assignment.feature_country.presentation.CountryEvent.Internal
import kotlinx.coroutines.flow.Flow
import vivid.money.elmslie.coroutines.Actor
import javax.inject.Inject

internal class CountryActor @Inject constructor(
    private val api: EsimApi,
) : Actor<CountryCommand, Internal> {

    override fun execute(command: CountryCommand): Flow<Internal> =
        when (command) {
            is GetCountries -> actorFlow {
                api.getCountries()
            }.mapEvents(Internal::GetCountriesSuccess, Internal::GetCountriesError)
        }
}
