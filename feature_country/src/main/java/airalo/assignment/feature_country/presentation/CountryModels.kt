package airalo.assignment.feature_country.presentation

import airalo.assignment.domain_esim.dto.Country
import vivid.money.elmslie.core.store.Store

typealias CountryStore = Store<CountryEvent, CountryEffect, CountryState>

data class CountryState(
    val countries: List<Country> = emptyList(),
    val isLoading: Boolean = true,
)

sealed interface CountryEvent {

    sealed interface Ui : CountryEvent {
        object System {
            object Init : Ui
        }

        object Click {
            data class CountryItem(val id: Int) : Ui
        }
    }

    sealed interface Internal : CountryEvent {
        data class GetCountriesSuccess(val countries: List<Country>) : Internal
        data class GetCountriesError(val error: Throwable) : Internal
    }
}

sealed interface CountryEffect {
    data class ShowError(val error: Throwable) : CountryEffect
    data class OpenPackages(val countryId: Int) : CountryEffect
}

sealed interface CountryCommand {
    object GetCountries : CountryCommand
}
