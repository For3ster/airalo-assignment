package airalo.assignment.feature_country.list

import airalo.assignment.feature_country.presentation.CountryState

internal object CountryListConverter {

    fun map(state: CountryState): List<CountryItem> =
        state.countries.map { country ->
            CountryItem(
                id = country.id,
                title = country.title,
                imageUrl = country.image.url
            )
        }
}
