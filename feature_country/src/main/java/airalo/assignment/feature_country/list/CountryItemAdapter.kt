package airalo.assignment.feature_country.list

import airalo.assignment.feature_country.databinding.ItemCountryBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

internal class CountryItemAdapter(
    private val onItemClick: (CountryItem) -> Unit,
) : ListAdapter<CountryItem, CountryItemViewHolder>(ItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemCountryBinding.inflate(inflater, parent, false)

        return CountryItemViewHolder(binding.root, onItemClick)
    }

    override fun onBindViewHolder(holder: CountryItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private class ItemDiffCallback : DiffUtil.ItemCallback<CountryItem>() {

    override fun areItemsTheSame(oldItem: CountryItem, newItem: CountryItem): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: CountryItem, newItem: CountryItem): Boolean =
        oldItem.title == newItem.title
}
