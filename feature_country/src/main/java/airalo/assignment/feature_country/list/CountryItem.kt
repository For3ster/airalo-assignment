package airalo.assignment.feature_country.list

data class CountryItem(
    val id: Int,
    val title: String,
    val imageUrl: String,
)
