package airalo.assignment.feature_country.list

import airalo.assignment.feature_country.databinding.ItemCountryBinding
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import coil.load

internal class CountryItemViewHolder(
    itemView: View,
    private val onItemClick: (CountryItem) -> Unit,
) : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemCountryBinding.bind(itemView)

    fun bind(item: CountryItem) = with(binding) {
        countryFlag.load(item.imageUrl)
        title.text = item.title
        root.setOnClickListener { onItemClick(item) }
    }
}
