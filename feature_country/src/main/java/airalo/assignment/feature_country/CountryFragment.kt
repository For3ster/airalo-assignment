package airalo.assignment.feature_country

import airalo.assignment.common_di.getComponent
import airalo.assignment.common_mvi.base.BaseFragment
import airalo.assignment.common_mvi.setOnBackPressedCallback
import airalo.assignment.common_navigation.router
import airalo.assignment.feature_country.databinding.FragmentCountryBinding
import airalo.assignment.feature_country.di.CountryDependencies
import airalo.assignment.feature_country.list.CountryItem
import airalo.assignment.feature_country.list.CountryItemAdapter
import airalo.assignment.feature_country.list.CountryListConverter
import airalo.assignment.feature_country.presentation.CountryEffect
import airalo.assignment.feature_country.presentation.CountryEvent.Ui
import airalo.assignment.feature_country.presentation.CountryState
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import vivid.money.elmslie.android.storeholder.LifecycleAwareStoreHolder
import vivid.money.elmslie.android.storeholder.StoreHolder
import airalo.assignment.feature_country.presentation.CountryEffect as Effect
import airalo.assignment.feature_country.presentation.CountryEvent as Event
import airalo.assignment.feature_country.presentation.CountryState as State

internal class CountryFragment : BaseFragment<Event, Effect, State>(R.layout.fragment_country) {

    private val component by lazy { getComponent<CountryDependencies>() }

    override val initEvent: Event = Ui.System.Init

    override val storeHolder: StoreHolder<Event, Effect, State> =
        LifecycleAwareStoreHolder(
            lifecycle = lifecycle,
            storeProvider = { component.countryStoreFactory.create() }
        )

    private val binding by viewBinding(FragmentCountryBinding::bind)
    private lateinit var adapter: CountryItemAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.adapter = createAdapter().also { adapter = it }
        setOnBackPressedCallback { router.exit() }
    }

    override fun render(state: CountryState) {
        binding.progressView.root.isVisible = state.isLoading
    }

    override fun mapList(state: CountryState): List<CountryItem> =
        CountryListConverter.map(state)

    @Suppress("UNCHECKED_CAST")
    override fun renderList(state: CountryState, list: List<Any>) {
        adapter.submitList(list as List<CountryItem>)
    }

    override fun handleEffect(effect: Effect) = when (effect) {
        is CountryEffect.OpenPackages -> {
            val screen = component.packageFeatureApi.getScreen(effect.countryId)
            router.navigateTo(screen)
        }

        is CountryEffect.ShowError -> showError(effect.error.toString())
    }

    private fun createAdapter() = CountryItemAdapter { item ->
        store.accept(Ui.Click.CountryItem(item.id))
    }

    companion object {

        fun newInstance(): Fragment = CountryFragment()
    }
}
