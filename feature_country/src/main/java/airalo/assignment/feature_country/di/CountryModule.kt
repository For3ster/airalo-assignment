package airalo.assignment.feature_country.di

import airalo.assignment.domain_country.feature_api.CountryFeatureApi
import airalo.assignment.feature_country.feature_api.CountryFeatureApiImpl
import airalo.assignment.feature_country.presentation.CountryStoreFactory
import airalo.assignment.feature_country.presentation.CountryStoreFactoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class CountryModule {

    @Binds
    internal abstract fun CountryFeatureApiImpl.bindCountryFeatureApi(): CountryFeatureApi

    @Binds
    internal abstract fun CountryStoreFactoryImpl.bindCountryStoreFactory(): CountryStoreFactory
}
