package airalo.assignment.feature_country.di

import airalo.assignment.domain_package.feature_api.PackageFeatureApi
import airalo.assignment.feature_country.presentation.CountryStoreFactory

interface CountryDependencies {

    val countryStoreFactory: CountryStoreFactory
    val packageFeatureApi: PackageFeatureApi
}
