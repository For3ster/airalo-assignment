package airalo.assignment.feature_country.presentation

import airalo.assignment.domain_esim.dto.Country
import airalo.assignment.feature_country.presentation.CountryCommand.GetCountries
import airalo.assignment.feature_country.presentation.CountryEffect.OpenPackages
import airalo.assignment.feature_country.presentation.CountryEffect.ShowError
import airalo.assignment.feature_country.presentation.CountryEvent.Internal
import airalo.assignment.feature_country.presentation.CountryEvent.Ui
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import kotlin.random.Random

internal class CountryReducerTest : BehaviorSpec({
    val reducer = CountryReducer

    Given("Initial State") {
        When("Ui.System.Init") {
            val (state, effects, commands) = reducer.reduce(Ui.System.Init, STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") {
                commands.shouldContainExactly(GetCountries)
            }
        }

        When("Internal.GetCountriesSuccess") {
            val (state, effects, commands) =
                reducer.reduce(Internal.GetCountriesSuccess(COUNTRIES), STATE)

            Then("check state") {
                state.countries shouldBe COUNTRIES
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.GetCountriesError") {
            val (state, effects, commands) =
                reducer.reduce(Internal.GetCountriesError(ERROR), STATE)

            Then("check state") {
                state.countries.shouldBeEmpty()
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldContainExactly(ShowError(ERROR)) }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }

    Given("Loaded countries") {
        val givenState = STATE.copy(countries = COUNTRIES, isLoading = false)

        When("Ui.Click.CountryItem") {
            val (state, effects, commands) =
                reducer.reduce(Ui.Click.CountryItem(COUNTRY.id), givenState)

            Then("check state") { state shouldBe givenState }
            Then("check effects") {
                effects.shouldContainExactly(OpenPackages(COUNTRY.id))
            }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }
}) {

    private companion object {

        val COUNTRY: Country = mock { on { it.id } doReturn Random.nextInt() }
        val COUNTRIES = List(3) { COUNTRY }
        val STATE = CountryState()
        val ERROR = Throwable()
    }
}
